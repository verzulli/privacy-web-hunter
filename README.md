# Privacy Web Hunter

**This software is released under the AGPL 3.0. Please, refer to [LICENSE.txt](LICENSE.txt) for details**

![](public_html/animated.gif)

What's this?
------------

This tool has been written as an exercise to check the "Privacy friendlyness" of a potentially wide-set of websites.

In short: **it's "automation" built on-top of [Privacy Badger](https://privacybadger.org/)**

I decided to write such an application from-scratch, in Node, taking into account the following:

* letting the whole application being [12-factor](https://12factor.net/) compliant
* embed the whole application logic within a unified engine, heavily modularized, to easy both the maintenance of the code and the containerization (the original code is splitted among several apps, interacting with shared files)
* export the data in JSON format
* support running along multiple sessions (save data / load data)
* exposing an CLI-interface, ready to be extended with a REST-API (and _YES_, you read correctly. I said CLI-interface (see below))

I hope this will easy the process, for other people, to reuse this whole stuff

----

Architecture
------------
The whole application is built around:
* a *daemon* the expose a CLI-interface binded to a standard TCP port
* a *crawling engine* based on [Privacy Badger](https://privacybadger.org/), that... kindly gives back us **LOTS** of useful privacy-infos
* a basis of *REST-api tools* to be later added on
* some glue code.

All the stuff have been written for NodeJS environment, in ES6 standard.

Code is transpiled with babel

A Dockerfile is included, ready to build a proper container containing the application bundle, ready to be served

----

How to run
----------
*(please, I'm going to be short, here. Don't hesitate to ask for details)*

**WARNING: an already existing Firefox Profile, with the [Privacy Badger](https://privacybadger.org/) extension already installed, is REQUIRED!!!!** - Should you need them, guidelines are [here](https://support.mozilla.org/en-US/kb/profile-manager-create-remove-switch-firefox-profiles). Please note that you need the ID of the extension. 

Once the profile is ready, and you have retrieved the ID of the Privacy Badger extension:

* clone the repo
* ensure you have a properly running NodeJS setup (note: I've developed everything under Linux)
* install dependencies with

      npm install

* configure the environment

      copy .env.sample .env

  and fine-tune `.env` based on your preferences
* start the server with 

      npm start

* access the console with 

      telnet localhost 2300

* exercise yourself with related commands. As a starter:

    * `refresh pa list` => to fetch the initial list
    * `start crawl` and `stop crawl` => to start/stop the crawling activities
    * `show pa list` => to see the list of PAs managed by the system
    * `dump data` => to print a JSON of the datastore
    * `save data` => to save the JSON to a local file
    * `load data` => to retrieve the datastore from an existing file

Access results
--------------
When crawling is complete, you can issue a `dump data` to get, in the CLI, the JSON formatted results, like:

    [
    {
        "web": "www.artemedia.srl",
        "results": {
        "startTime": 1645993920346,
        "pbResult": {
            "host": "www.artemedia.srl",
            "url": "https://www.artemedia.srl/main/",
            "origins": {
            "maps.googleapis.com": "noaction",
            "maps.gstatic.com": "cookieblock",
            "www.google-analytics.com": "block",
            "www.google.com": "cookieblock"
            }
        },
        "stopTime": 1645993925431,
        "success": true
        },
        "processed": true
    },
    {
        "web": "www.unich.it",
        "results": {
        "startTime": 1645993925432,
        "pbResult": {
            "host": "www.unich.it",
            "url": "https://www.unich.it/",
            "origins": {
            "cse.google.com": "cookieblock",
            "fonts.googleapis.com": "noaction",
            "fonts.gstatic.com": "cookieblock",
            "i.ytimg.com": "noaction",
            "ka-f.fontawesome.com": "noaction",
            "kit.fontawesome.com": "noaction",
            "www.google-analytics.com": "block",
            "www.google.com": "cookieblock",
            "www.youtube-nocookie.com": "cookieblock",
            "yt3.ggpht.com": "noaction"
            }
        },
        "stopTime": 1645993932335,
        "success": true
        },
        "processed": true
    }
    ]

where, for each element of the resulting array, you have Privacy-Badge results in the *pbResult* structure.

Further info
------------
You can reach me via:
* opening issues, here, on this very project
* e-mail me at damiano [at] verzulli [dot] it
* Telegram: [@Damiano_Verzulli](https://t.me/Damiano_Verzulli)