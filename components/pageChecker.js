/* Privacy Web-Hunter
   Copyright (C) 2022 - Damiano Verzulli <damiano@verzulli.it>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require('dotenv').config()
import logger from '../components/logger'
import { Builder, By } from 'selenium-webdriver'
import firefox from 'selenium-webdriver/firefox'


/* 
* a proposito di jsInjectedDetector, a regime lo sostituiremo con la struttura POPUP_DATA (o POPUP_DATA.origins) 
* che ci mette a disposizione Privacy Badger, alla URL:
* chrome-extension://pkehgijcmpdhfbdbbnkijodmdjhbjlgp/skin/popup.html (chiaramente da parametrizzare)
*/

let _driver
const PBOPTIONURL = process.env.FIREFOX_PROFILE
const WEBPAGELOADTIMEOUT = process.env.WEBPAGE_LOAD_TIMEOUT

// sleep: needed to simulate asynchronous webpage load
function sleep(t) {
  let ms = 0
  if (typeof (t) === 'number') {
    ms = t
  } else {
    ms = Math.trunc(Math.random() * 2000)
  }
  return new Promise((resolve) => setTimeout(resolve, ms))
}

async function clickConsentButton() {
  // let's track if we'll click or not (aka: if we found some "ccett" buttons.. or not)
  let clicked = false

  // XPath - thanks to Mauro Gorrino
  // BTW: ti search for _ALL_ <button...>[string]</button> where [string]
  //      match "accett" (matched after lowercasing A, C, E, T)
  let consentPath = "//button[contains(translate(., 'ACET', 'acet'), 'accett')]"
  try {
    let buttons = await _driver.findElements(By.xpath(consentPath))
    if (typeof (buttons) === 'object') {
      for await (const button of buttons) {
        await button.click()
        clicked = true
      }
    }
    logger.info("[clickConsentButton] Button clicked!")
  } catch (e) {
    logger.error("[clickConsentButton] Error: [" + e + "]")
  }
  return (clicked)
}

export default {

  initFirefox: async function () {
    logger.info("[pageChecker/initFirefox] initializing Firefox driver...")
    let profile = '/home/verzulli/.mozilla/firefox/03ri47mu.selenium'

    // let options = new firefox.Options().setProfile(profile).headless()
    let options = new firefox.Options().setProfile(profile)

    try {
      _driver = await new Builder().
        forBrowser('firefox').
        setFirefoxOptions(options).
        build()
      logger.info("[pageChecker/initFirefox] Initialization done!")
      await sleep(100)
      return true
    } catch (e) {
      logger.error("[pageChecker/initFirefox] Error initializing webdriver [" + e + "]")
      return false
    }
  },

  process: async (url) => {
    // data structure to hold process results
    let result = {}

    // let's do some sanification of the incoming URL
    let realUrl = url.toLowerCase()
    if (!url.startsWith('http')) {
      realUrl = 'http://' + url
    }
    if (url === realUrl) {
      logger.info("[pageChecker/loadPage] Loading [" + url + "]")
    } else {
      logger.info("[pageChecker/loadPage] Loading [" + realUrl + "] instead of source [" + url + "]")
    }

    //let's track some data
    result.startTime = Date.now()
    result.realUrl = realUrl

    // Ready. Let's "open" the window...
    try {

      // let's load the URL

      // Let's define a promise, raising an error after 10 seconds!
      let timeoutPromise = new Promise(function (resolve, reject) {
        setTimeout(() => reject(new Error('Load page timeout!')), WEBPAGELOADTIMEOUT * 1000)
      })

      // Let's define the promise related to our async loading of the URL
      let webPageLoadPromise = _driver.get(realUrl)

      // Let's start the _RACE_ between the two promises...
      await Promise.race([timeoutPromise, webPageLoadPromise])

      // let's add a little more...
      await sleep(100)
  
      // let's remember the handle for this "main" tab/window...
      let mainTab = _driver.getWindowHandle()

      // let's click the "consent button" (..if present)
      // As clickConsentButton is an "async" function, it's return value
      // cannot be retrieved normali (let val = clickConsentButton()) but
      // we need to explicitely rely on a callback
      let clicked = await clickConsentButton()

      // let's track if we clicked or not...
      result.accettClicked = clicked
      logger.info("[pageChecker/loadPage] clicked: [" + result.accettClicked + "]")

      // let's wait a little, after clicking (even if "click" is synchronized)
      await sleep(100)

      logger.info("[pageChecker/loadPage] Let's go to PB... (on a new tab!)")
      await _driver.switchTo().newWindow('tab')
      await sleep(100)
      await _driver.get(PBOPTIONURL)
      await sleep(100)
  
      logger.info("[pageChecker/loadPage] Here we are...")
      let tmpResults = await _driver.executeScript(`
      let tabData = browser.extension.getBackgroundPage().badger.tabData;
      console.log(JSON.stringify(tabData));
      return tabData;
      `)
      result.pbResult = {}
      result.pbResult.host = tmpResults[1].frames[0].host
      result.pbResult.url = tmpResults[1].frames[0].url
      result.pbResult.origins = tmpResults[1].origins

      logger.info("[pageChecker/loadPage] Fatto! =>" + JSON.stringify(result) + "<=")

      // close PB TAB and go back to main tab
      await _driver.executeScript(`window.close();`)
      await sleep(100)
      await _driver.switchTo().window(mainTab)

      result.stopTime = Date.now()
      result.success = true
    } catch (err) {
      logger.error('[pageChecker/loadPage] error: [' + err + ']')
      result.success = false
      if (typeof (err) === 'object') {
        result.lastError = err.message
      } else {
        result.lastError = err
      }

    }

    return result
  }
}