/* Privacy Web-Hunter
   Copyright (C) 2022 - Damiano Verzulli <damiano@verzulli.it>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require('dotenv').config()
import logger from '../components/logger'
import axios from 'axios'
import fs from 'fs'
import pageChecker from '../components/pageChecker.js'

let _paList = []            // this is the main array of retrieved PAs
let _stopRequested = false  // true, to request STOP crawling

export default {

  connected: false,

  init: async () => {
    logger.info('[crawler/init] Loading cached data file... if it exists')

    let cacheFile = process.env.TMPFILE_PA
    try {
      // reset existing content
      _paList = []

      // read data from file
      let tmpStr = fs.readFileSync(cacheFile)
      let tmpList = JSON.parse(tmpStr)

      // destructure content, to populate real in-memory data-structure
      for await (const paArray of tmpList) {
        let pa = {
          'id': paArray[1],
          'name': paArray[2],
          'pec': paArray[20] + '|' + paArray[19],
          'web': paArray[29]
        }
        _paList.push(pa)
      }
      logger.info('[crawler/init] Loaded [' + _paList.length + '] entities from cache file [' + cacheFile + ']')

      // initializing pageCheckers
      logger.info('[crawler/init] Initializing pageChecker')
      let res = await pageChecker.initFirefox()
      if (res) {
        logger.info('[crawler/init] pageChecker initializazione done!')
      } else {
        logger.error('[crawler/init] Error initializing browser driver')
      }
      return res
    } catch (e) {
      logger.error('[crawler/init] Error initializing crawler [' + e + ']')
      return false
    }
  },

  paListReady: () => {
    return _paList.length > 0
  },

  getPaList: () => {
    return _paList
  },

  setPaList: (paList) => {
    _paList = paList
  },

  refreshPAList: async () => {
    let URL = process.env.URL_PA
    let fileName = process.env.TMPFILE_PA
    try {
      let page = await axios({
        method: 'GET',
        url: URL,
        headers: {
        },
        timeout: 20 * 1000, // 20... seconds
      })
      // let's check if we have our expected data structure
      if (typeof (page.data.records) === 'object') {
        let tmp = page.data.records
        
        let textContent = JSON.stringify(tmp)
        fs.writeFileSync(fileName, textContent)

        await this.init()
        logger.info('[crawler/refreshPAList] retrieved [' + _paList.length + '] entities for [' + textContent.length + ' bytes written in [' + fileName + ']')
        let msg = _paList.length + ' entities retrieved'
        return (msg)
      } else {
        return (false)
      }
    } catch (err) {
      logger.error('[crawler/refreshPAList] error: [' + err + ']')
      return (false)
    }
  },

  stopCrawling: () => {
    _stopRequested = true
  },

  startCrawl: async () => {
    logger.info('[crawler/startCrawl] starting crawling')
    for await (const pa of _paList) {
      // while iterating, let's check if someone, via Console, requested a "stop"
      if (_stopRequested) {
        logger.info('[crawler/startCrawl] STOP receiveid. Terminating crawling')
        _stopRequested = false
        break
      }

      // let's crawl, but _ONLY_ if we haven't already done so... (in 10 days)
      let curTime = Date.now()
      // have we succesfully crawled before?
      if (typeof(pa.processed) !== 'undefined' && typeof(pa.results) !== 'undefined' && typeof(pa.results.startTime) !== 'undefined') {
        if (( curTime - pa.results.startTime) < 10 * 24 * 60 * 60 * 1000 ) {
          // this PA is to skip, as we already dealt with it
          logger.info('[crawler/startCrawl] NOT crawling for [' + pa.id + '] as we already have data: >>' + JSON.stringify(pa.results) + "<<")
        } else {
          // this PA is to be processed, as our data are more than 10 days old. 
          logger.info('[crawler/startCrawl] Recrawling for [' + pa.id + '] as our data are old: >>' + JSON.stringify(pa.results) + "<<")
          pa.results = await pageChecker.process(pa.web)
          pa.processed = true
          logger.info('[crawler/startCrawl] Recrawling for [' + pa.id + '] terminated: >>' + JSON.stringify(pa.results) + '<<')
        }
      } else {
        // this PA is to be processed, as we don't have its data
        logger.info('[crawler/startCrawl] crawling for the NEW [' + pa.id + '] started')
        pa.results = await pageChecker.process(pa.web)
        pa.processed = true
        logger.info('[crawler/startCrawl] crawling for [' + pa.id + '] terminated: >>' + JSON.stringify(pa.results) + '<<')
      }
    }
    return true
  }
}