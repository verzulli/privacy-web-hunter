/* Privacy Web-Hunter
   Copyright (C) 2022 - Damiano Verzulli <damiano@verzulli.it>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require('dotenv').config()
import logger from '../components/logger'
import os from 'os'

export default {
  ping: (req, res) => {
    let data = {};
    data.inRequestParams = req.body
    data.inRequestUrl = req.url

    logger.info('[WS/testMethod] received params [' + JSON.stringify(data.inRequestParams) + ']');
    logger.info('[WS/testMethod] received url [' + JSON.stringify(data.inRequestUrl) + ']');

    res.json(data)
  },

  home: (req, res) => {
    logger.info('[WS/home] serving home. My name is [' + os.hostname() + ']')
    res.send('<H1>Running on: <B>' + os.hostname() + '</B></H1>Please refer to content (index.html) <A href="/static">here</A>')
  }

}