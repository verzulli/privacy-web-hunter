/* Privacy Web-Hunter
   Copyright (C) 2022 - Damiano Verzulli <damiano@verzulli.it>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import logger from './logger.js'
import net from 'net'
import cliHandlers from './cliHandlers.js'

export default {

  curClientID: 0,
  curConsoleClientID: 0,
  serverConsolePort: null,
  serverConsoleAddress: null,
  clients: [],
  consoleClients: [],

  startConsoleServer(consolePort, address, prompt) {
    let engineConsole = this
    engineConsole.serverConsolePort = consolePort
    engineConsole.serverConsoleAddress = address
    engineConsole.serverConsolePrompt = prompt

    logger.info('[cliConsole/startConsoleServer] Starting console server: [' + engineConsole.serverConsoleAddress + '/' + engineConsole.serverConsolePort + ']');
    engineConsole.consoleConnection = net.createServer((socket) => { engineConsole.onConsoleClientConnected(socket) })
    engineConsole.consoleConnection.listen(engineConsole.serverConsolePort, engineConsole.serverConsoleAddress)
  },

  onConsoleClientConnected(socket) {
    let engineConsole = this

    logger.info('[cliConsole/onConsoleClientConnected] New client connected: [' + socket.remoteAddress + '/' + socket.remotePort + ']')
    engineConsole.curConsoleClientID++

    // let's add the "ID reference" downstream to client socket (so to match it, afterward, to retrieve its own preferences)
    socket.logMonID = engineConsole.curConsoleClientID

    // Identify this client and assign "default" preferences
    let preferences = {
      "console": true,
      "debug": true
    }
    let logmonConsoleClient = {
      "id": engineConsole.curConsoleClientID,
      "name": "Console Client: [" + socket.remoteAddress + ":" + socket.remotePort + "]",
      "preferences": preferences,
      "socket": socket
    }

    // Add this new client to the "consoleClients" array
    engineConsole.consoleClients.push(logmonConsoleClient)    

    // Send a nice welcome message and announce
    socket.write("****************************************************\n")
    socket.write("***      Privacy WEB-HUNTER - Console Server     ***\n")
    socket.write("****************************************************\n")
    socket.write("\n")
    socket.write(this.serverConsolePrompt + " ")

    // Handle incoming messages from clients.
    socket.on('data', (data) => {
      let engineConsole = this
      engineConsole.onConsoleSocketData(socket, data)
    })

    // Remove the client from the list when it leaves
    socket.on('end', () => {
      let engineConsole = this
      engineConsole.onConsoleSocketEnd(socket)
    })

    socket.on('error', (err) => {
      logger.error('[cliConsole/onConsoleClientConnected] Error connecting with client [' + err.message + ']. Retrying in 5 sec')
    })

  },
  onConsoleSocketData(socket, inputData) {

    // let's get rid of leading and/or trailing spaces
    let command = String(inputData).replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '').replace(/[^A-Za-z0-9_ ]+/g, '').toLowerCase()

    logger.debug('[cliConsole/onConsoleSocketData] Processing ==>' + command + '<===')

    // let's process the command, unless it's a "logout"
    if (command !== 'logout' && command !== 'exit' && command !== 'quit' ) {

      // ...and we simply ignore empty commands
      if (command !== '') {

        // ok. So we got a command (aka: something typed in the CLI). Let's see
        // if it's a valid command (aka: a command "mapped" to some action)

        // let's check if we have a "mapped methods" associated to the command
        let handlerFound = []
        for (let idx=0; idx < cliHandlers.commandMap.length; idx++) {
            let k = cliHandlers.commandMap[idx]
            if (command.search(k.regexp) !== -1 ) {
              handlerFound.push(k)
            }
        }
        
        // Have we found a "matching" handler?
        if (Array.isArray(handlerFound) && handlerFound.length > 0) {

          // yes. We found something...

          if (handlerFound.length == 1) {
            // We found exactly 1 handler! Let's de-reference the related routine
            logger.debug('[cliConsole/onConsoleSocketData] Found handler for ==>' + command + '<===')

            // here is our "stanza" with information regarding what we have to do..
            let handler = handlerFound[0]

            // and how... let's "call" it, passing "socket" as the argument
            logger.info('[cliConsole/onConsoleSocketData] Executing ==>' + handler.handler + '<===')
            cliHandlers[handler.handler](socket, command)

          } else {
            // we found more than one handler. Let's tell the user...
            logger.info('[cliConsole/onConsoleSocketData] Too many handlers found ==>' + command + '<===')
            cliHandlers.ambiguousCommand(socket)
          }
        } else {
          // we haven't found an handler for what has been typed to che CLI
          // let's tell the user...
          logger.info('[cliConsole/onConsoleSocketData] Handler NOT found for ==>' + command + '<===')
          cliHandlers.unknownCommand(socket)
        }
        socket.write("\n");
      }
      socket.write(this.serverConsolePrompt + " ")

    } else {
      // we got a "logout/exit" so... let's close the socket
      socket.end()
    }

  },

  onConsoleSocketEnd(socket) {
    let engineConsole = this
    logger.info('[cliConsole/onConsoleSocketEnd] Client [' + socket.remoteAddress + '/' + socket.remotePort + '] disconnected!')
    // Let's remove this client from the "consoleClients" array
    engineConsole.consoleClients.splice(this.consoleClients.indexOf(socket), 1)
  },

  broadcast(msg) {
    let engineConsole = this
    logger.debug('cliConsole/broadcast] broadcasting msg [' + msg + ']')
    engineConsole.consoleClients.forEach( (client) => {if (client.preferences.console) { client.socket.write(msg + "\n")}})
  }
}