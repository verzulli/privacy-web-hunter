/* Privacy Web-Hunter
   Copyright (C) 2022 - Damiano Verzulli <damiano@verzulli.it>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require('dotenv').config()
import logger from '../components/logger.js'
import cliConsoleServer from './cliServer.js'
import { sprintf } from 'sprintf-js'
import crawler from './crawler.js'
import fs from 'fs'

export default {
  commandMap: [
    {
      "command": "help",
      "regexp": /help/,
      "handler": "showHelp",
      "description": "Show help/usage"
    },
    {
      "command": "show version",
      "regexp": /show version/,
      "handler": "showVersion",
      "description": "Show version number"
    },
    {
      "command": "show clients",
      "regexp": /show clients/,
      "handler": "showClients",
      "description": "Show clients connected to this concole engine"
    },
    {
      "command": "show preferences",
      "regexp": /show preferences/,
      "handler": "showPreferences",
      "description": "Show current setting for preferences (console, keepalive, etc.)"
    },
    {
      "command": "console <on|off>",
      "regexp": /console/,
      "handler": "console",
      "description": "Enable/Disable display of remote console output"
    },
    {
      "command": "show crawler status",
      "regexp": /show crawler status/,
      "handler": "showCrawlerStatus",
      "description": "Show crawler status"
    },
    {
      "command": "refresh pa list",
      "regexp": /refresh pa list/,
      "handler": "refreshPAList",
      "description": "Refresh the list of PAs, by retrieving (and saving) new file"
    },
    {
      "command": "show pa list",
      "regexp": /show pa list/,
      "handler": "showPAList",
      "description": "SHOW the list of PAs, as stored in the system"
    },
    {
      "command": "start crawl",
      "regexp": /start crawl/,
      "handler": "startCrawl",
      "description": "start the crawling of the whole set of web pages"
    },
    {
      "command": "stop crawl",
      "regexp": /stop crawl/,
      "handler": "stopCrawl",
      "description": "stop the crawling activity"
    },
    {
      "command": "dump data",
      "regexp": /dump data/,
      "handler": "dumpData",
      "description": "dump the datastore in JSON format"
    },
    {
      "command": "save data",
      "regexp": /save data/,
      "handler": "saveData",
      "description": "save current datastore to file, in JSON format"
    },
    {
      "command": "load data",
      "regexp": /load data/,
      "handler": "loadData",
      "description": "load current datastore to in-memory structure"
    }

  ],
  showHelp(socket, command) {
    logger.debug('[PWH/CLI/showHelp] show help')
    socket.write("\n")
    this.commandMap.forEach((handler) => {
      socket.write(sprintf("%25s: %s\n", handler.command, handler.description))
    })
    socket.write(sprintf("%25s: %s\n", "exit | quit", "exit the console, leaving everything running"))
    socket.write("\n")
  },

  showVersion(socket, command) {
    logger.debug('[PWH/CLI/showVersion] show version')
    socket.write("Privacy-WEB-Hunter - v. <unknown>!\n") // TODO
    socket.write("(c) 2022 - Damiano Verzulli - damiano@verzulli.it\n")
    socket.write("Released under the GPL-v3 terms of License\n")
    socket.write("\n")
    socket.write("Started on: [ <to be added> ]\n") // TODO
    // let uptime = Math.round((Date.now() - STARTED_ON_MS) / 1000)
    socket.write("Uptime....: [ unknown ] sec") // TODO
  },

  showClients(socket, command) {
    logger.debug('[PWH/CLI/showClients] ...')
    let counter = 1
    socket.write(sprintf(" num  client_id  Connected from\n"))
    cliConsoleServer.consoleClients.forEach((client) => {
      let msg = sprintf("[%3d] %9s %s\n", counter++, client.id, client.name)
      socket.write(msg)
    }
    )
    socket.write("\n")
  },

  showPreferences(socket, command) {
    socket.write("Here are current preferences:\n")
    cliConsoleServer.consoleClients.forEach((client) => {
      if (client.id == socket.logMonID) {
        for (let pref in client.preferences) {
          let msg = sprintf("[%20s]: [%s]\n", pref, client.preferences[pref])
          socket.write(msg)
        }
        socket.write("\n")
      }
    })
  },

  // this is called directly from cliConsoleServer, to enable/disable console messages
  console(socket, command) {
    let commandParam = command.slice(8).toUpperCase() // let's get rid of "console " part of the command
    let msg = ''
    cliConsoleServer.consoleClients.forEach((client) => {
      if (client.id == socket.logMonID) {
        if (commandParam == 'ON') {
          client.preferences.console = true
          msg = 'remote console messages, ENABLED!'
          logger.debug('[PWH/CLI/console] remote console for [' + client.id + '] enabled')
        } else if (commandParam == 'OFF') {
          client.preferences.console = false
          msg = 'remote console messages, DISABLED!'
          logger.debug('[PWH/CLI/console] remote console for [' + client.id + '] disabled')
        } else {
          msg = 'Current remote console messages status for [' + client.id + '] is: [' + client.preferences.console + ']'
        }
        socket.write(msg + "\n")
      }
    })
  },

  showCrawlerStatus(socket, command) {
    socket.write("..to be implemented...")
    // socket.write("Here are current nodes status:\n")
    
    // // looppo sui nodi...
    // let _now = Date.now();
    // for (const [_node, _lastSeen] of Object.entries(_status)) {
    //   socket.write("\nNode: [" + ... + "]\n")
    //   let msg = sprintf("+-> [%d] seconds ago\n", Math.floor((_now - _lastSeen ) / 1000))
    //   socket.write(msg)
    // }
  },

  refreshPAList: async (socket, command) => {
    socket.write("(refresh) Requesting refresh list... (asynchronously)")
    try {
      let res = await crawler.refreshPAList()
      socket.write("(refresh) Done! [" + res + "]\n")
      await crawler.init()
    } catch (e) {
      socket.write("(refresh) Error! [" + e + "]")
    }
  },

  showPAList: async (socket, command) => {
    socket.write("Dumping the list of (currently loaded) PAs...")
    try {
      let counter = 0
      let listaPa = crawler.getPaList()
      for await (const pa of listaPa) {
          let msg = sprintf("[%8s]: [%s]\n            email1: [%s]\n            WEB...: [%s]\n", 
            pa.id, // IPA Code
            pa.name.substring(0,49), // administration name
            pa.pec, // 1st e-mail
            pa.web // website
            )
          if (typeof(pa.processed) !== 'undefined' && pa.processed) {
            let timeSpent = Math.round((pa.results.stopTime - pa.results.startTime) / 1000)
            msg += "            LastCrawl => " + timeSpent + " sec. - Result: [" + pa.results.success + "]\n"
          }
          socket.write(msg)
          counter++
      }
      socket.write("==> Total Number of PAs reported: [" + counter + "]\n")
    } catch (e) {
      socket.write("Error dumping list!")
    }
  },

  startCrawl: async (socket, command) => {
    logger.info("[cliHandlers/startCrawl] Starting the crawling")
    socket.write("Starting the crawling")
    try {
      let res = await crawler.startCrawl()
      socket.write("Done! [" + res + "]")
      logger.info("[cliHandlers/startCrawl] Done! [" + res + "]")
    } catch (e) {
      logger.error("[cliHandlers/startCrawl] Error! [" + e + "]")
      socket.write("Error occurred firing main crawling!")
    }
  },

  stopCrawl: (socket, command) => {
    logger.info("[cliHandlers/stopCrawl] Stopping the crawling")
    socket.write("Stopping the crawling")
    try {
      let res = crawler.stopCrawling()
      socket.write("Done! [" + res + "]")
    } catch (e) {
      logger.error("[cliHandlers/stopCrawl] Error! [" + e + "]")
      socket.write("Error occurred requesting stop crawling!")
    }
  },

  dumpData: async (socket, command) => {
    logger.info("[PWH/CLI/dumpData] Dumping datastore in JSON format")
    socket.write("Dumping the datastore...\nvvvvvvv JSON BEGIN vvvvvvvv\n")
    try {
      let counter = 0
      let listaPa = crawler.getPaList()
      socket.write(JSON.stringify(listaPa))
      socket.write("\n^^^^^^^^^^^ JSON END ^^^^^^^^^\n")
      socket.write("==> Total Number of PAs reported: [" + counter + "]\n")
    } catch (e) {
      socket.write("Error dumping list!")
      logger.errpr("[PWH/CLI/dumpData] Error: [" + e + "]")
    }
  },

  saveData: async (socket, command) => {
    let fileName = process.env.DATASTORE || '.datastore.json'
    logger.info("[PWH/CLI/saveData] Saving datastore in JSON format to [" + fileName + "]")
    socket.write("Saving the datastore to [" + fileName + "]")
    try {
      let listaPa = crawler.getPaList()
      let fileContent = JSON.stringify(listaPa)
      let fileSize = fileContent.length
      fs.writeFileSync(fileName, fileContent)
      socket.write("Saved! Written [" + fileSize + "] bytes to [" + fileName + "]\n")
    } catch (e) {
      socket.write("Error saving list!")
      logger.errpr("[PWH/CLI/saveData] Error: [" + e + "]")
    }
  },

  loadData: async (socket, command) => {
    let fileName = process.env.DATASTORE || '.datastore.json'
    logger.info("[PWH/CLI/loadData] Loading datastore from JSON file [" + fileName + "]")
    socket.write("Loading the JSON datastore from [" + fileName + "]")
    try {
      let fileContent = fs.readFileSync(fileName)
      let listaPa = JSON.parse(fileContent)
      let numPa = listaPa.length
      let fileSize = fileContent.length
      crawler.setPaList(listaPa)
      socket.write("Loaded [" + numPa + "] entities, from a [" + fileSize + "] bytes on-disk datastore!\n")
    } catch (e) {
      socket.write("Error saving list!")
      logger.errpr("[PWH/CLI/saveData] Error: [" + e + "]")
    }
  },

  // this is called directly from cliConsoleServer, as a "default"
  unknownCommand(socket, command) {
    logger.debug('[PWH/CLI/unknownCommand] ...')
    socket.write('Unknown command....')
  },

  // this is called directly from cliConsoleServer, as a "default"
  ambiguousCommand(socket, command) {
    logger.debug('[PWH/CLI/aunkmbiguousCommand] ...')
    socket.write('Ambiguous command! Please restrict it....')
  }
}
