/* Privacy Web-Hunter
   Copyright (C) 2022 - Damiano Verzulli <damiano@verzulli.it>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const winston = require('winston')

const tsFormat = () => (new Date().toISOString())

const myFormat = winston.format.printf(
  ({ level, message, label, timestamp }) => {
      return `${timestamp} [${label}] ${level}: ${message}`;
  }
)

const logger = winston.createLogger({
  level: 'debug',
  format: myFormat
})

logger.add(new winston.transports.Console({
    format: winston.format.combine(
        winston.format.timestamp({ format: 'DD-MM-YYYY HH:mm:ss.SSS'}),
        winston.format.label({ label: 'Privacy-WEB-HUNTER' }),
        myFormat
    )

}))

export default logger
