/* Privacy Web-Hunter
   Copyright (C) 2022 - Damiano Verzulli <damiano@verzulli.it>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require('dotenv').config({path: __dirname + '/.env'})
import express from 'express'
import logger from './components/logger'
import appRoutes from './components/routes.js'
import cors from 'cors'
import path from 'path'
import serveStatic from 'serve-static'
import cliConsole from './components/cliServer.js'
import crawler from './components/crawler.js'

// define listening REST-API TCP port: 3001, unless defined in config
let restApiPort = process.env.REST_API_PORT || 3001

// define listening "console" (telnet) port: 2323, unless defined in config
let consolePort = process.env.CONSOLE_PORT || 2323

logger.info('[MAIN] Starting app')

const app = express()
const router = express.Router()
app.use(cors())

// per richieste POST...
app.use(express.json())
app.use(express.urlencoded({
  extended: true
}))

app.options('*', cors()) // per CORS e pre-flight
app.use('/', appRoutes(router))
app.use('/static', serveStatic(path.join(__dirname, 'public_html'),{ 'index': ['index.html', 'index.htm'] }))

app.listen(restApiPort, function () {
  logger.info('[MAIN] API-server listening on port [' + restApiPort + ']')
})

logger.info('[MAIN] Initializing CONSOLE SERVER...')
cliConsole.startConsoleServer(consolePort, '0.0.0.0', 'pWebHunter>')

logger.info('[MAIN] Initializing Crawler...')
crawler.init()
